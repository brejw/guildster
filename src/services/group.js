const Group = require("../models/Group");
const Event = require("../models/Event");

const addGroup = async (req, res) =>
{
    try {
        const groupInfo = req.body;
        const group = new Group(groupInfo);
        await group.save();

        res.status(201).send(group);
    } catch (err) {
        res
            .status(400)
            .send({ code: 'ADD_GROUP_ERR', message: 'Cannot add group' });
    }
};

const getGroup = async (req, res) =>
{
    try {
        const group = await Group.find({
            status: true
        }).
            populate("tanks").
            populate("heals").
            populate("dps").
            exec();
        res.status(200).send(group);
    } catch (err) {
        res.status(400).send({
            code: "GET_GROUP_ERR",
            message: "Cannot get groups"
        });
    }
};

const addCharacterToGroup = async (req, res) =>
{
    try {

        const groupId = req.body.id;
        const role = req.body.role;
        const characterId = req.body.characterId;
        switch (role) {
            case "tanks":
                const tank = await Group.findById(groupId).exec();
                const tanksArray = tank.tanks;
                tanksArray.push(characterId)
                Group.findByIdAndUpdate(groupId, { tanks: tanksArray }).exec();
                break;
            case "heals":
                const heal = await Group.findById(groupId).exec();
                const healsArray = heal.heals;
                healsArray.push(characterId)
                Group.findByIdAndUpdate(groupId, { heals: healsArray }).exec();
                break;
            case "dps":
                const dpss = await Group.findById(groupId).exec();
                const dpssArray = dpss.dps;
                dpssArray.push(characterId)
                Group.findByIdAndUpdate(groupId, { dps: dpssArray }).exec();
                break;
        }
        res.status(204).send(groupId);
    } catch (err) {
        res
            .status(400)
            .send({ code: 'ADD_CHAR_GROUP_ERR', message: 'Cannot add character' });
    }
};

const removeGroup = async (req, res) =>
{
    try {
        const groupId = req.params.id;

        Group.findByIdAndRemove(groupId).exec();

        res.status(204).send();
    } catch (err) {
        res
            .status(400)
            .send({ code: 'REMOVE_GROUP_ERR', message: 'Cannot remove group' });
    }
};

const updateGroup = async (req, res) =>
{
    try {
        const groupId = req.params.id;
        const group = req.body;

        Group.findByIdAndUpdate(groupId, group).exec();

        res.status(201).send();
    } catch (err) {
        res.status(400).send({
            code: "UPDATE_GROUP_ERR",
            message: "Cannot update group"
        });
    }
};

const addGroupToEvent = async (req, res) =>
{
    try {

        const groupId = req.body.groupId;
        const eventId = req.body.eventId;

        const event = await Event.findById(eventId).exec();
        const group = await Group.findById(groupId).exec();

        tanksArray = event.tanks.concat(group.tanks);
        Event.findByIdAndUpdate(eventId, { tanks: tanksArray }).exec();

        healsArray = event.heals.concat(group.heals);
        Event.findByIdAndUpdate(eventId, { heals: healsArray }).exec();

        dpsArray = event.dps.concat(group.dps);
        Event.findByIdAndUpdate(eventId, { dps: dpsArray }).exec();

        res.status(204).send(groupId);
    } catch (err) {
        res
            .status(400)
            .send({ code: 'ADD_GROUP_EVENT_ERR', message: 'Cannot add group to event' });
    }
};

module.exports = {
    addGroup,
    getGroup,
    addCharacterToGroup,
    removeGroup,
    updateGroup,
    addGroupToEvent
};
