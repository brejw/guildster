const mongoose = require('mongoose');

const CharacterSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  origin: {
    type: String,
    required: true
  },
  realm: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: ['healer', 'dps', 'tank'],
    required: true
  }
});
module.exports = mongoose.model('Character', CharacterSchema);
