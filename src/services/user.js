const jwt = require('jsonwebtoken');
const User = require('../models/User');
const Character = require('../models/Character');
const Application = require('../models/Application');

function generateToken(user) {
  return jwt.sign(user, process.env.SECRET || 'abc', {
    expiresIn: '24h'
  });
}

function setUserInfo(request) {
  return {
    _id: request._id,
    email: request.email,
    role: request.role,
    characters: request.characters
  };
}

const login = (req, res, next) => {
  const userInfo = setUserInfo(req.user);

  res.status(200).send({
    token: generateToken(userInfo),
    user: userInfo
  });
};

const register = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const characters = req.body.characters;
  const applicationData = req.body.application;

  if (!email) {
    return res.status(422).send({ error: 'You must enter an email address' });
  }

  if (!password) {
    return res.status(422).send({ error: 'You must enter a password' });
  }

  User.findOne({ email }, function(err, existingUser) {
    if (err) {
      return next(err);
    }

    if (existingUser) {
      return res
        .status(422)
        .send({ error: 'That email address is already in use' });
    }

    const user = new User({
      email: email,
      password: password
    });

    const savedCharactersIds = [];

    for (let characterInfo of characters) {
      const character = new Character(characterInfo);
      character.save();
      savedCharactersIds.push(character._id);
    }
    user.characters = savedCharactersIds;

    user.save(function(err, user) {
      if (err) {
        return next(err);
      }
      const application = new Application(applicationData);
      application.user = user._id;
      application.save();

      res.status(201).send({ message: 'USER_CREATED' });
    });
  });
};

const roleAuthorization = roles => {
  return (req, res, next) => {
    const user = req.user;

    User.findById(user._id, function(err, foundUser) {
      if (err) {
        res.status(422).json({ error: 'No user found.' });
        return next(err);
      }

      if (roles.indexOf(foundUser.role) > -1) {
        return next();
      }

      res
        .status(401)
        .send({ error: 'You are not authorized to view this content' });
      return next('Unauthorized');
    });
  };
};

module.exports = {
  register,
  login,
  roleAuthorization
};
