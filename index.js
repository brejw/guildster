const app = require('./src/server');

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));


/*
Funkcjonalności:
    + Rejestracja (aplikowanie do gildii) / logowanie użytkownika - T
    + Dodawanie swoich postaci do konta - T
    + Planowanie raidów / dungeonów / eventów- B
    + Tworzenie grup raidowych - B
    - Wyświetlanie obecnego progressu gildii - B
    - Forum wewnętrzne
*/