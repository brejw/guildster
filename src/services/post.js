const Post = require('../models/Post');

const getPosts = () => {
  return Post.find();
};
const getPost = id => {
  return Post.findById(id);
};

const addPost = postDto => {
  const post = new Post(postDto);

  return post.save();
};

const updatePost = (id, postDto) => {
  return Post.findByIdAndUpdate(id, postDto).exec();
};

const removePost = id => {
  return Post.findByIdAndRemove(id, function(err, post) {
    post.remove();
  }).exec();
};

module.exports = {
  getPosts,
  getPost,
  addPost,
  updatePost,
  removePost
};
