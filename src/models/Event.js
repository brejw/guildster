const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EventSchema = new Schema(
    {
        name: String,
        type: {
            type: String,
            enum: ['raid', 'dungeon', 'pvp', 'other'],
            default: 'other'
        },
        date: {
            type: Date,
            default: Date.now
        },
        author: String,
        tanks: [{ type: Schema.Types.ObjectId, ref: "Character" }],
        heals: [{ type: Schema.Types.ObjectId, ref: "Character" }],
        dps: [{ type: Schema.Types.ObjectId, ref: "Character" }],
        pending: [{ type: Schema.Types.ObjectId, ref: "Character" }],
        status: {
            type: Boolean,
            default: true
        }
    }
)

module.exports = mongoose.model('Event', EventSchema);