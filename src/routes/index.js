const { Router } = require("express");
const userRoutes = require("./user");
const characterRoutes = require("./character");
const eventRoutes = require("./event");
const groupRoutes = require("./group");
const categoryRoutes = require('./category');
const topicRoutes = require('./topic');
const postRoutes = require('./post');
const apiRoutes = Router();

apiRoutes.use("/auth", userRoutes);
apiRoutes.use("/character", characterRoutes);
apiRoutes.use("/event", eventRoutes);
apiRoutes.use("/group", groupRoutes);
apiRoutes.use('/category', categoryRoutes);
apiRoutes.use('/topic', topicRoutes);
apiRoutes.use('/post', postRoutes);

module.exports = apiRoutes;
