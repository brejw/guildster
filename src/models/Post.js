const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const PostSchema = new Schema(
  {
    content: {
      type: String,
      required: true
    },
    topicId: String,
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true }
  },
  {
    timestamps: true
  }
);

PostSchema.pre('remove', function(next) {
  this.model('Topic').update(
    {},
    { $pull: { posts: this._id } },
    { multi: true },
    next
  );
});
module.exports = mongoose.model('Post', PostSchema);
