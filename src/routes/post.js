const { Router } = require('express');
const topicService = require('../services/topic');
const postService = require('../services/post');
const passport = require('passport');
const postApi = Router();
const requireAuth = passport.authenticate('jwt', { session: false });

postApi.get('/', requireAuth, async (req, res) => {
  try {
    const id = req.query.id;

    if (id) {
      const post = await postService.getPost(id);
      res.json(post);
    } else {
      const posts = await postService.getPosts();
      res.json(posts);
    }
  } catch (err) {
    res.status(400).send({
      code: 'GET_POST_ERR',
      message: 'Cannot get posts'
    });
  }
});
postApi.post('/', requireAuth, async (req, res) => {
  try {
    const postDto = req.body;
    const { topicId } = req.body;
    const post = await postService.addPost(postDto);
    const topic = await topicService.getTopic(topicId);
    topic.posts.push(post._id);
    await topicService.updateTopic(topicId, topic);

    res.json(post);
  } catch (err) {
    res.status(400).send({
      code: 'ADD_POST_ERR',
      message: 'Cannot add post'
    });
  }
});
postApi.put('/:id', requireAuth, async (req, res) => {
  try {
    await postService.updatePost(req.params.id, req.body);

    res.status(204).send();
  } catch (err) {
    res.status(400).send({
      code: 'UPDATE_POST_ERR',
      message: 'Cannot update post'
    });
  }
});
postApi.delete('/:id', requireAuth, async (req, res) => {
  try {
    await postService.removePost(req.params.id);

    res.status(204).send();
  } catch (err) {
    res.status(400).send({
      code: 'REMOVE_POST_ERR',
      message: 'Cannot remove post'
    });
  }
});
module.exports = postApi;
