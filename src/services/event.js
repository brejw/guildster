const Event = require("../models/Event");

const addEvent = async (req, res) =>
{
    try {
        const eventInfo = req.body;
        const event = new Event(eventInfo);
        await event.save();

        res.status(201).send(event);
    } catch (err) {
        res
            .status(400)
            .send({ code: 'ADD_EVENT_ERR', message: 'Cannot add event' });
    }
};

const getEvent = async (req, res) =>
{
    try {
        const event = await Event.find({
            status: true
        }).
            populate("tanks").
            populate("heals").
            populate("dps").
            populate("pending").
            exec();
        res.status(200).send(event);
    } catch (err) {
        res.status(400).send({
            code: "GET_EVENT_ERR",
            message: "Cannot get events"
        });
    }
};

const addCharacterToEvent = async (req, res) =>
{
    try {

        const eventId = req.body.id;
        const role = req.body.role;
        const characterId = req.body.characterId;
        switch (role) {
            case "tanks":
                const tank = await Event.findById(eventId).exec();
                const tanksArray = tank.tanks;
                tanksArray.push(characterId)
                Event.findByIdAndUpdate(eventId, { tanks: tanksArray }).exec();
                break;
            case "heals":
                const heal = await Event.findById(eventId).exec();
                const healsArray = heal.heals;
                healsArray.push(characterId)
                Event.findByIdAndUpdate(eventId, { heals: healsArray }).exec();
                break;
            case "dps":
                const dpss = await Event.findById(eventId).exec();
                const dpssArray = dpss.dps;
                dpssArray.push(characterId)
                Event.findByIdAndUpdate(eventId, { dps: dpssArray }).exec();
                break;
            default:
                const pendings = await Event.findById(eventId).exec();
                const pendingsArray = pendings.pending;
                pendingsArray.push(characterId)
                Event.findByIdAndUpdate(eventId, { pending: pendingsArray }).exec();
                break;
        }
        res.status(204).send(eventId);
    } catch (err) {
        res
            .status(400)
            .send({ code: 'ADD_CHAR_EVENT_ERR', message: 'Cannot add character' });
    }
};

const removeEvent = async (req, res) =>
{
    try {
        const eventId = req.params.id;

        Event.findByIdAndRemove(eventId).exec();

        res.status(204).send();
    } catch (err) {
        res
            .status(400)
            .send({ code: 'REMOVE_EVENT_ERR', message: 'Cannot remove event' });
    }
};

const updateEvent = async (req, res) =>
{
    try {
        const eventId = req.params.id;
        const event = req.body;

        Event.findByIdAndUpdate(eventId, event).exec();

        res.status(201).send();
    } catch (err) {
        res.status(400).send({
            code: "UPDATE_EVENT_ERR",
            message: "Cannot update event"
        });
    }
};

module.exports = {
    getEvent,
    addEvent,
    removeEvent,
    addCharacterToEvent,
    updateEvent
};
