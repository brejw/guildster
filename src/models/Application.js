const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ApplicationSchema = new Schema({
  topic: {
    type: String,
    required: true
  },
  content: String,
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  status: {
    type: String,
    enum: ['new', 'processed', 'accepted', 'rejected'],
    default: 'new'
  }
});
const autoPopulate = function(next) {
  this.populate('user');
  next();
};

ApplicationSchema.pre('find', autoPopulate).pre('findOne', autoPopulate);
module.exports = mongoose.model('Application', ApplicationSchema);
