const { Router } = require("express");
const groupService = require("../services/group");
const passport = require("passport");
const groupApi = Router();
const requireAuth = passport.authenticate("jwt", { session: false });

groupApi.get("/", requireAuth, groupService.getGroup);
groupApi.post("/", requireAuth, groupService.addGroup);
groupApi.delete("/:id", requireAuth, groupService.removeGroup);
groupApi.post("/add", requireAuth, groupService.addCharacterToGroup);
groupApi.post("/update/:id", requireAuth, groupService.updateGroup);
groupApi.post("/addgrouptoevent", requireAuth, groupService.addGroupToEvent);

module.exports = groupApi;