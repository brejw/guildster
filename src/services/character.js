const Character = require('../models/Character');

const getCharacter = async (req, res) => {
  const blizzard = require('blizzard.js').initialize({
    apikey: process.env.BLIZZARD_API_KEY
  });
  try {
    const characterInfo = req.query;

    const character = await blizzard.wow.character(['profile'], characterInfo);

    res.status(200).send(character.data);
  } catch (err) {
    console.log(err.response);
    res
      .status(400)
      .send({ code: 'GET_CHAR_ERR', message: 'Cannot get character info' });
  }
};
const addCharacter = async (req, res) => {
  try {
    const characterInfo = req.body;

    const character = new Character(characterInfo);
    await character.save();

    res.status(201).send(character);
  } catch (err) {
    res
      .status(400)
      .send({ code: 'ADD_CHAR_ERR', message: 'Cannot add character' });
  }
};

const removeCharacter = async (req, res) => {
  try {
    const characterId = req.params.id;

    Character.findByIdAndRemove(characterId).exec();

    res.status(204).send();
  } catch (err) {
    res
      .status(400)
      .send({ code: 'REMOVE_CHAR_ERR', message: 'Cannot remove character' });
  }
};

module.exports = {
  getCharacter,
  addCharacter,
  removeCharacter
};
