const { Router } = require('express');
const topicService = require('../services/topic');
const categoryService = require('../services/category');
const passport = require('passport');
const topicApi = Router();
const requireAuth = passport.authenticate('jwt', { session: false });

topicApi.get('/', requireAuth, async (req, res) => {
  try {
    const id = req.query.id;

    if (id) {
      const topic = await topicService.getTopic(id);
      res.status(200).send(topic);
    } else {
      const topics = await topicService.getTopics();
      res.status(200).send(topics);
    }
  } catch (err) {
    res.status(400).send({
      code: 'GET_TOPIC_ERR',
      message: 'Cannot get topics'
    });
  }
});
topicApi.post('/', requireAuth, async (req, res) => {
  try {
    const topicDto = req.body;
    const { categoryId } = req.body;
    const topic = await topicService.addTopic(topicDto);
    const category = await categoryService.getCategory(categoryId);
    category.topics.push(topic._id);

    await categoryService.updateCategory(categoryId, category);

    res.status(201).send(topic);
  } catch (err) {
    res.status(400).send({
      code: 'ADD_TOPIC_ERR',
      message: 'Cannot add topic'
    });
  }
});
topicApi.put('/:id', requireAuth, async (req, res) => {
  try {
    await topicService.updateTopic(req.params.id, req.body);

    res.status(204).send();
  } catch (err) {
    res.status(400).send({
      code: 'UPDATE_TOPIC_ERR',
      message: 'Cannot update topic'
    });
  }
});
topicApi.delete('/:id', requireAuth, async (req, res) => {
  try {
    await topicService.removeTopic(req.params.id);

    res.status(204).send();
  } catch (err) {
    res.status(400).send({
      code: 'REMOVE_TOPIC_ERR',
      message: 'Cannot remove topic'
    });
  }
});
module.exports = topicApi;
