const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  parentId: String,
  categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
  topics: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Topic'
    }
  ]
});

CategorySchema.pre('remove', function(next) {
  this.model('Category').remove({ parentId: this._id }, next);
});

module.exports = mongoose.model('Category', CategorySchema);
