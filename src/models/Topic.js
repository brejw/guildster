const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TopicSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  categoryId: String,
  posts: [{ type: Schema.Types.ObjectId, ref: 'Post' }]
});

const autoPopulate = function(next) {
  this.populate('posts');
  next();
};

TopicSchema.pre('find', autoPopulate).pre('findOne', autoPopulate);

TopicSchema.pre('remove', function(next) {
  this.model('Category').update(
    {},
    { $pull: { topics: this._id } },
    { multi: true }
  );
  this.model('Post').remove({ postId: this._id }, next);
});
module.exports = mongoose.model('Topic', TopicSchema);
