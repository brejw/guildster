const Topic = require('../models/Topic');

const getTopics = () => {
  return Topic.find();
};
const getTopic = id => {
  return Topic.findById(id);
};

const addTopic = topicDto => {
  const topic = new Topic(topicDto);

  return topic.save();
};

const updateTopic = (id, topicDto) => {
  return Topic.findByIdAndUpdate(id, topicDto).exec();
};

const removeTopic = id => {
  return Topic.findByIdAndRemove(id, function(err, topic) {
    topic.remove();
  }).exec();
};

module.exports = {
  getTopics,
  getTopic,
  addTopic,
  updateTopic,
  removeTopic
};
