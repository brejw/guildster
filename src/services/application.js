const Application = require("../models/Application");

const getApplications = async (req, res) => {
  try {
    const applications = await Application.find({
      status: { $in: ["new", "processing"] }
    }).populate("User");

    res.status(200).send(applications);
  } catch (err) {
    res.status(400).send({
      code: "GET_APPLICATIONS_ERR",
      message: "Cannot get applications"
    });
  }
};

const updateApplication = async (req, res) => {
  try {
    const applicationId = req.params.id;
    const application = req.body;

    Application.findByIdAndUpdate(applicationId, application).exec();

    res.status(201).send();
  } catch (err) {
    res.status(400).send({
      code: "UPDATE_APPLICATION_ERR",
      message: "Cannot update application"
    });
  }
};

module.exports = {
  getApplications,
  updateApplication
};
