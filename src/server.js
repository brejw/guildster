const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./routes');
require('dotenv').load();
require('./config/passport');

const app = express();
mongoose
  .connect(
    process.env.MONGO_DB_URL,
    { useNewUrlParser: true }
  )
  .then(() => console.log('Connected to DB'))
  .catch(err => console.log('Error occured while connecting to DB'));
mongoose.Promise = global.Promise;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(cors());
app.use('/api', routes);

module.exports = app;
