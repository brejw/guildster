const userService = require("../../src/services/user");

describe("User service tests", () => {
  it("should login successfully", () => {
    const req = {
      user: {
        _id: "abc",
        email: "test@test.test",
        role: "member"
      }
    };
    const mockRes = jest.fn({
      status: jest.fn(status => {
        this.status = status;
        return this;
      }),
      send: jest.fn(payload => {
        this.payload = payload;
        return this;
      })
    });
    const next = jest.fn();
    const res = new mockRes();
    userService.login(req, res, next);

    expect(res.status).toBe(200);
  });
});
