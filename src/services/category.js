const Category = require('../models/Category');
const { removeTopic } = require('./topic');
const getCategories = () => {
  return Category.find().populate('Topic');
};

const getCategory = id => {
  return Category.findById(id).populate('topics');
};

const addCategory = async categoryDto => {
  const category = new Category(categoryDto);

  await category.save();

  return category;
};

const updateCategory = (id, categoryDto) => {
  return Category.findByIdAndUpdate(id, categoryDto).exec();
};

const removeCategory = id => {
  /*const category = await Category.findById(id)
    .populate('Category')
    .populate('Topic');

  for (const topic of category.topics) {
    removeTopic(topic);
  }
  for (const category of category.categories) {
    removeCategory(category._id);
  }*/
  return Category.findOneAndDelete({ _id: id }, function(err, category) {
    category.remove();
  }).exec();
};

module.exports = {
  getCategories,
  getCategory,
  addCategory,
  updateCategory,
  removeCategory
};
