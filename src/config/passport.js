const passport = require('passport');
const User = require('../models/User');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;

const localOptions = {
  usernameField: 'email',
  passwordField: 'password'
};

const localLogin = new LocalStrategy(localOptions, function(
  email,
  password,
  done
) {
  User.findOne(
    {
      email
    },
    function(err, user) {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done(null, false, { error: 'User not found' });
      }

      user.comparePassword(password, function(err, isMatch) {
        if (err) {
          return done(err);
        }

        if (!isMatch) {
          return done(null, false, {
            error: 'Invalid login or password.'
          });
        }

        return done(null, user);
      });
    }
  );
});

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.SECRET
};

const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
  User.findById(payload._id, function(err, user) {
    if (err) {
      return done(err, false);
    }

    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  }).populate('Characters');
});

passport.use(jwtLogin);
passport.use(localLogin);
