const { Router } = require('express');
const userService = require('../services/user');
const passport = require('passport');
const userApi = Router();
const requireLogin = passport.authenticate('local', { session: false });
userApi.post('/login', requireLogin, userService.login);
userApi.post('/register', userService.register);

module.exports = userApi;
