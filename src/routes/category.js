const { Router } = require('express');
const categoryService = require('../services/category');
const passport = require('passport');
const categoryApi = Router();
const requireAuth = passport.authenticate('jwt', { session: false });

categoryApi.get('/', requireAuth, async (req, res) => {
  try {
    const id = req.query.id;

    if (id) {
      const category = await categoryService.getCategory(id);
      res.status(200).send(category);
    } else {
      const categories = await categoryService.getCategories();
      res.status(200).send(categories);
    }
  } catch (err) {
    res.status(400).send({
      code: 'GET_CATEGORY_ERR',
      message: 'Cannot get categories'
    });
  }
});
categoryApi.post('/', requireAuth, async (req, res) => {
  try {
    const categoryDto = req.body;
    const { parentId } = req.body;
    const category = await categoryService.addCategory(categoryDto);
    if (parentId) {
      const parentCategory = await categoryService.getCategory(parentId);
      parentCategory.categories.push(category._id);

      await categoryService.updateCategory(parentId, parentCategory);
    }

    res.status(201).send(category);
  } catch (err) {
    res.status(400).send({
      code: 'ADD_CATEGORY_ERR',
      message: 'Cannot add category'
    });
  }
});
categoryApi.put('/:id', requireAuth, async (req, res) => {
  try {
    await categoryService.updateCategory(req.params.id, req.body);

    res.status(204).send();
  } catch (err) {
    res.status(400).send({
      code: 'UPDATE_CATEGORY_ERR',
      message: 'Cannot update category'
    });
  }
});
categoryApi.delete('/:id', requireAuth, async (req, res) => {
  try {
    await categoryService.removeCategory(req.params.id);

    res.status(204).send();
  } catch (err) {
    res.status(400).send({
      code: 'REMOVE_CATEGORY_ERR',
      message: 'Cannot remove category'
    });
  }
});
module.exports = categoryApi;
