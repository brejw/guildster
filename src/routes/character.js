const { Router } = require("express");
const characterService = require("../services/character");
const passport = require("passport");
const characterApi = Router();
const requireAuth = passport.authenticate("jwt", { session: false });

characterApi.get("/", characterService.getCharacter);
characterApi.post("/", requireAuth, characterService.addCharacter);
characterApi.delete("/:id", requireAuth, characterService.removeCharacter);

module.exports = characterApi;
