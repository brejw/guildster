const { Router } = require("express");
const eventService = require("../services/event");
const passport = require("passport");
const eventApi = Router();
const requireAuth = passport.authenticate("jwt", { session: false });

eventApi.get("/", requireAuth, eventService.getEvent);
eventApi.post("/", requireAuth, eventService.addEvent);
eventApi.delete("/:id", requireAuth, eventService.removeEvent);
eventApi.post("/add", requireAuth, eventService.addCharacterToEvent);
eventApi.post("/update/:id", requireAuth, eventService.updateEvent);

module.exports = eventApi;
